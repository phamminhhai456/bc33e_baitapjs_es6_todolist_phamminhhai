import {
  getEle,
  getInfo,
  renderTasks,
  stringToSlug,
} from "./controller/taskController.js";

let tasks = [];

const BASE_URL = `https://62f8b7553eab3503d1da15e2.mockapi.io/`;

let renderTasksService = () => {
  axios({
    url: `${BASE_URL}/todoList`,
    method: "GET",
  })
    .then((res) => {
      tasks = res.data;
      renderTasks(tasks);
    })
    .catch((err) => {});
};
renderTasksService();

let deleteTask = (id) => {
  axios({
    url: `${BASE_URL}/todoList/${id}`,
    method: "DELETE",
  })
    .then((res) => {
      renderTasksService();
    })
    .catch((err) => {});
};
window.deleteTask = deleteTask;

let addTask = () => {
  let dataForm = getInfo();
  axios({
    url: `${BASE_URL}/todoList`,
    method: "POST",
    data: dataForm,
  })
    .then((res) => {
      getEle("#newTask").value = "";
      renderTasksService();
    })
    .catch((err) => {});
};

window.addTask = addTask;

getEle("#addItem").addEventListener("click", () => {
  addTask();
});

let updateStatus = (data, id) => {
  let dataForm = data;

  if (dataForm.status == false) {
    dataForm.status = true;
  } else {
    dataForm.status = false;
  }
  axios({
    url: `${BASE_URL}/todoList/${id}`,
    method: "PUT",
    data: dataForm,
  })
    .then((res) => {
      renderTasksService();
    })
    .catch((err) => {});
};

let editTask = (id) => {
  axios({
    url: `${BASE_URL}/todoList/${id}`,
    method: "GET",
  })
    .then((res) => {
      updateStatus(res.data, id);
    })
    .catch((err) => {});
};

window.editTask = editTask;

let sortAZ = () => {
  tasks.sort((a, b) => {
    if (
      stringToSlug(a.task).toUpperCase() > stringToSlug(b.task).toUpperCase()
    ) {
      return 1;
    }
    if (
      stringToSlug(a.task).toUpperCase() < stringToSlug(b.task).toUpperCase()
    ) {
      return -1;
    }
    return 0;
  });
  renderTasks(tasks);
};

window.sortAZ = sortAZ;

let sortZA = () => {
  tasks.sort((a, b) => {
    if (
      stringToSlug(a.task).toUpperCase() < stringToSlug(b.task).toUpperCase()
    ) {
      return 1;
    }
    if (
      stringToSlug(a.task).toUpperCase() > stringToSlug(b.task).toUpperCase()
    ) {
      return -1;
    }
    return 0;
  });
  renderTasks(tasks);
};

window.sortZA = sortZA;
