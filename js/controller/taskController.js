export let getEle = (tag) => {
  return document.querySelector(tag);
};

export let renderTasks = (tasks) => {
  let taskFinished = ``;
  let taskUnfinished = ``;

  tasks.forEach((item) => {
    let taskRow = `
        <li>
            <span>${item.task}</span>
            <span>
            <i onclick="deleteTask(${item.id})" class="fa fa-trash-alt"></i> <i onclick="editTask(${item.id})" class="fa fa-check-circle"></i>
            </span>
        </li>
    `;
    if (item.status == false) {
      taskUnfinished += taskRow;
    } else {
      taskFinished += taskRow;
    }

    getEle("#todo").innerHTML = taskUnfinished;
    getEle("#completed").innerHTML = taskFinished;
  });
};

export let getInfo = () => {
  let task = getEle("#newTask").value;
  let status = false;
  return { task, status };
};

export let stringToSlug = (str) => {
  // remove accents
  var from =
      "àáãảạăằắẳẵặâầấẩẫậèéẻẽẹêềếểễệđùúủũụưừứửữựòóỏõọôồốổỗộơờớởỡợìíỉĩịäëïîöüûñçýỳỹỵỷ",
    to =
      "aaaaaaaaaaaaaaaaaeeeeeeeeeeeduuuuuuuuuuuoooooooooooooooooiiiiiaeiiouuncyyyyy";
  for (var i = 0, l = from.length; i < l; i++) {
    str = str.replace(RegExp(from[i], "gi"), to[i]);
  }

  str = str
    .toLowerCase()
    .trim()
    .replace(/[^a-z0-9\-]/g, " ")
    .replace(/-+/g, " ");

  return str;
};
